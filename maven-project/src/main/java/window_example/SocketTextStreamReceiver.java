package window_example;

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.dstream.NetworkReceiver;
import scala.reflect.ClassTag;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketTextStreamReceiver extends NetworkReceiver {
    private String host;
    private Integer port;
    private BlockGenerator bg;
    private Socket s;

    public SocketTextStreamReceiver(ClassTag evidence$2) {
        super(evidence$2);
    }

    void SocketTextStreamReceiver(String in_host, Integer in_port) {
        this.host = in_host;
        this.port = in_port;
        this.bg = new BlockGenerator(this, StorageLevel.MEMORY_AND_DISK_SER_2());
    }

    @Override
    public void onStart() {
        try {
            bg.start();
            this.s = new Socket(this.host, this.port);
            s.setReceiveBufferSize(10000000);
            BufferedReader dataInputStream = new BufferedReader(new InputStreamReader(s.getInputStream()));

            String data = dataInputStream.readLine();
            while (data != null) {
                bg.$plus$eq(data);
                data = dataInputStream.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        bg.stop();
    }
}

