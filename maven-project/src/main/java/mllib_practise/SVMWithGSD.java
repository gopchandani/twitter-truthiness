/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package mllib_practise;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.classification.SVMModel;
import org.apache.spark.mllib.classification.SVMWithSGD;
import org.apache.spark.mllib.regression.LabeledPoint;

import scala.Tuple2;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Logistic regression based classification using ML Lib.
 */
public final class SVMWithGSD {

    static class ParsePoint extends Function<String, LabeledPoint> {
        private static final Pattern COMMA = Pattern.compile(",");
        private static final Pattern SPACE = Pattern.compile(" ");

        @Override
        public LabeledPoint call(String line) {
            String[] parts = COMMA.split(line);
            double y = Double.parseDouble(parts[0]);
            String[] tok = SPACE.split(parts[1]);
            double[] x = new double[tok.length];
            for (int i = 0; i < tok.length; ++i) {
                x[i] = Double.parseDouble(tok[i]);
            }
            return new LabeledPoint(y, x);
        }
    }

    public static void printWeights(double[] a) {
        System.out.println(Arrays.toString(a));
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Usage: SVMWithGSD <master> <input_dir/file> <niters>");
            System.exit(1);
        }

        JavaSparkContext sc = new JavaSparkContext(args[0], "SVMWithGSD",
                System.getenv("SPARK_HOME"), JavaSparkContext.jarOfClass(SVMWithGSD.class));

        JavaRDD<String> lines = sc.textFile(args[1]);
        JavaRDD<LabeledPoint> points = lines.map(new ParsePoint()).cache();
        int iterations = Integer.parseInt(args[2]);



        final SVMModel model = SVMWithSGD.train(points.rdd(), iterations);

        JavaPairRDD<Double, Double> labelAndPreds = points.map(
                new PairFunction<LabeledPoint, Double, Double>() {
                    @Override
                    public Tuple2<Double, Double> call(LabeledPoint point) {
                        Double prediction = model.predict(point.features());
                        return new Tuple2<Double, Double>(point.label(), prediction);
                    }
                });


        System.out.print("Final w: ");
        printWeights(model.weights());


        /*Compute Training Error */

        JavaPairRDD<Double, Double> trainErr = labelAndPreds.filter(
                new Function<Tuple2<Double, Double>, Boolean> () {

                    @Override
                    public Boolean call(Tuple2<Double, Double> arg0)
                            throws Exception {

                        if (arg0._1 == arg0._2) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });

        long ratio = trainErr.count() / labelAndPreds.count();

        System.out.println(trainErr.collect());

        System.out.println(trainErr.count());
        System.out.println(labelAndPreds.count());
        System.out.print("Training error: " + ratio);

        System.exit(0);
    }
}
