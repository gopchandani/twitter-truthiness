package mllib_practise;


import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.classification.SVMModel;
import org.apache.spark.mllib.classification.SVMWithSGD;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.json.DataObjectFactory;
import wordcount.JavaTwitterWordCount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import features.TwitterTruthAnalyzer;
import org.apache.spark.api.java.JavaRDD;



public final class TweetClassifyMLLIB {


    static class ParsePoint extends Function<String, LabeledPoint> {
        private static final Pattern COMMA = Pattern.compile(",");

        @Override
        public LabeledPoint call(String line) {

            String[] parts = COMMA.split(line);
            TwitterTruthAnalyzer extractor = new TwitterTruthAnalyzer();

            double y = 0.0;
            double[] x = new double[parts.length - 1];
            int i = 0;
            while (i < parts.length - 1) {
                x[i] = Double.parseDouble(parts[i]);
                i++;
            }

            y = Double.parseDouble(parts[i]);

            LabeledPoint retPoint = new LabeledPoint(y, x);
            System.out.println(retPoint);
            return retPoint;
        }
    }

    private TweetClassifyMLLIB() {
    }

    public static void printWeights(double[] a) {
        System.out.println(Arrays.toString(a));
    }

    public static void main(String[] args) {


        if (args.length != 5) {
            System.err.println("Usage: TweetClassifyMLLIB <master> <input_dir/file> <niters> <hostname> <port>");
            System.exit(1);
        }

	JavaSparkContext sc = new JavaSparkContext(args[0], "TweetClassifyMLLIB",
                System.getenv("SPARK_HOME"), new String[]{"target/spark-streams-1.0-SNAPSHOT-jar-with-dependencies.jar"});
        JavaRDD<String> lines = sc.textFile(args[1]);

        JavaRDD<LabeledPoint> points = lines.map(new ParsePoint()).cache();


        int iterations = Integer.parseInt(args[2]);
        final SVMModel model = SVMWithSGD.train(points.rdd(), iterations);

        JavaPairRDD<Double, Double> labelAndPreds = points.map(
                new PairFunction<LabeledPoint, Double, Double>() {
                    @Override
                    public Tuple2<Double, Double> call(LabeledPoint point) {
                        Double prediction = model.predict(point.features());
                        return new Tuple2<Double, Double>(point.label(), prediction);
                    }
                });


        System.out.print("Final w: ");
        printWeights(model.weights());


        /*Compute Training Error */

        JavaPairRDD<Double, Double> trainErr = labelAndPreds.filter(
                new Function<Tuple2<Double, Double>, Boolean> () {

                    @Override
                    public Boolean call(Tuple2<Double, Double> arg0)
                            throws Exception {

                        if (arg0._1.compareTo(arg0._2) == 1) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });

        long ratio = trainErr.count() / labelAndPreds.count();

        System.out.println(trainErr.collect());

        System.out.println(trainErr.count());
        System.out.println(labelAndPreds.count());
        System.out.print("Training error: " + ratio);
	sc.stop();



        /* Now take the model created above and apply it to a stream */


        // Create the context with a 1 second batch size

        Duration batching_period = new Duration(1 * 1000);
	JavaStreamingContext ssc = new JavaStreamingContext(args[0],
                "TweetClassifyMLLIB", batching_period,
                System.getenv("SPARK_HOME"), new String[]{"target/spark-streams-1.0-SNAPSHOT-jar-with-dependencies.jar"});

        // Create a NetworkInputDStream on target ip:port
        JavaDStream<String> json_lines = ssc.socketTextStream(args[3],
                Integer.parseInt(args[4]));


        // Create a new stream that contains lines that contain just the text of
        // each tweet.
        JavaDStream<Status> statuses = json_lines
                .map(new Function<String, Status>() {
                    @Override
                    public Status call(String status_json) {
                        Status st = null;
                        try {
                            st = DataObjectFactory.createStatus(status_json);

                        } catch (TwitterException e) {
                            e.printStackTrace();
                        }

                        return st;
                    }
                });


		/* First filter for tweets */
        JavaDStream<Status> filtered_statuses = statuses.filter(
                new Function<Status, Boolean>() {
                    public Boolean call(Status st) {

                        if (st.getUser().getLang() == "en") {
                            return false;
                        } else
                        {
                            return true;
                        }
                    }
                }
        );

        /* Form a window of length 1 second */
        JavaDStream<Status> filtered_windowed_statuses = filtered_statuses.window(batching_period);


        JavaPairDStream<Double, Status> status_with_label = filtered_windowed_statuses.map(
                new PairFunction<Status, Double, Status>() {
                    @Override
                    public Tuple2<Double, Status> call(Status s) {
                        TwitterTruthAnalyzer extractor = new TwitterTruthAnalyzer();
                        Double label = 0.0;
                        try {
                             label = model.predict(extractor.extractOnlineFeaturesForMLLIB(s));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return new Tuple2<Double, Status> (label, s);
                    }
                });


        JavaPairDStream<Double, Status> suspicious_status = status_with_label.filter(
                new Function<Tuple2<Double, Status>, Boolean> () {

                    @Override
                    public Boolean call(Tuple2<Double, Status> arg0)
                            throws Exception {

                        if (arg0._1 == 1.0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }

                }
        );

        //filtered_windowed_statuses.print();

        status_with_label.print();
        suspicious_status.print();


        ssc.checkpoint("data-cache");
        ssc.start();
        ssc.awaitTermination();



        System.exit(0);

    }
}


