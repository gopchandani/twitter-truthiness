package mllib_practise;


import features.TwitterTruthAnalyzer;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.classification.LogisticRegressionModel;
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD;
import org.apache.spark.mllib.classification.SVMModel;
import org.apache.spark.mllib.classification.SVMWithSGD;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.json.DataObjectFactory;

import java.util.Arrays;
import java.util.regex.Pattern;


public final class TweetClassifyMLLIB_LR {


    static class ParsePoint extends Function<String, LabeledPoint> {
        private static final Pattern COMMA = Pattern.compile(",");

        @Override
        public LabeledPoint call(String line) {

            String[] parts = COMMA.split(line);
            TwitterTruthAnalyzer extractor = new TwitterTruthAnalyzer();

            double y = 0.0;
            double[] x = new double[parts.length - 1];
            int i = 0;
            while (i < parts.length - 1) {
                x[i] = Double.parseDouble(parts[i]);
                i++;
            }

            y = Double.parseDouble(parts[i]);

            LabeledPoint retPoint = new LabeledPoint(y, x);
            System.out.println(retPoint);
            return retPoint;
        }
    }

    private TweetClassifyMLLIB_LR() {
    }

    public static void printWeights(double[] a) {
        System.out.println(Arrays.toString(a));
    }

    public static void main(String[] args) {


        if (args.length != 5) {
            System.err.println("Usage: TweetClassifyMLLIB_LR <master> <input_dir/file> <niters> <hostname> <port>");
            System.exit(1);
        }

	    JavaSparkContext sc = new JavaSparkContext(args[0], "TweetClassifyMLLIB_LR",
                System.getenv("SPARK_HOME"), JavaSparkContext.jarOfClass(TweetClassifyMLLIB_LR.class));

        JavaRDD<String> lines = sc.textFile(args[1]);

        JavaRDD<LabeledPoint> points = lines.map(new ParsePoint()).cache();


        int iterations = Integer.parseInt(args[2]);
        final LogisticRegressionModel lr_model = LogisticRegressionWithSGD.train(points.rdd(), iterations, 10);

        JavaPairRDD<Double, Double> labelAndPreds = points.map(
                new PairFunction<LabeledPoint, Double, Double>() {
                    @Override
                    public Tuple2<Double, Double> call(LabeledPoint point) {
                        Double prediction = lr_model.predict(point.features());
                        //System.out.println(point.label() + " " + prediction);
                        return new Tuple2<Double, Double>(point.label(), prediction);
                    }
                }
        );


        System.out.print("Final w: ");
        printWeights(lr_model.weights());

        /* Compute Training Error */

        JavaPairRDD<Double, Double> trainErr = labelAndPreds.filter(
                new Function<Tuple2<Double, Double>, Boolean> () {

                    @Override
                    public Boolean call(Tuple2<Double, Double> arg0)
                            throws Exception {

                        Double label = arg0._1;
                        Double prediction = arg0._2;

                        if (label.compareTo(prediction) == 1) {
                            return true;

                        } else {
                            return false;
                        }
                    }
                });


        System.out.println("Training error: " + (trainErr.count() * 1.0 ) / labelAndPreds.count());
	    sc.stop();


        System.exit(0);

    }
}


