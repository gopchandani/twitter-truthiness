package velocity;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.io.IOException;
import java.io.File;

public class replay {

    private String folderPath;
    private File folderFile;
    private int serverPort;
    private int tweets_per_second;
    private int num_seconds;
    private ServerSocket servertSocket;
    private int dataSourceType;
    private String wordCountFilePath;

    public replay(String fp, int sp, int tps, int ns, int ds_type, String wcFilePath) {
        folderPath = fp;
        serverPort = sp;
        folderFile = new File(folderPath);
        tweets_per_second = tps;
        num_seconds = ns;
        dataSourceType = ds_type;
        wordCountFilePath = wcFilePath;
    }

    private void spawn_client(Socket clientSocket) {

        BlockingQueue<Tweet> queue = new ArrayBlockingQueue<Tweet>(tweets_per_second);
        TweetProducer producer = new TweetProducer(queue, folderFile,
                dataSourceType, tweets_per_second * num_seconds);

        TweetConsumer consumer = new TweetConsumer(queue, clientSocket,
                tweets_per_second, num_seconds, wordCountFilePath);

        // starting producer to produce messages in queue
        Thread producerThread = new Thread(producer);
        producerThread.start();

        // starting consumer to consume messages from queue
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();

        System.out.println("Producer and Consumer has been started");

		/*
		 * ClientWorker w = new ClientWorker(clientSocket, folderFile,
		 * tweets_per_second); Thread t = new Thread(w); t.start();
		 */
    }

    private void start_server() throws IOException {
        servertSocket = new ServerSocket(serverPort);
        try {
            while (true) {
                System.out.println("Waiting for client to connect...");
                Socket clientSocket = servertSocket.accept();

                spawn_client(clientSocket);

            }
        } catch (IOException e) {
            System.out.println("Accept failed: 4444");
            System.exit(-1);
        } finally {
            servertSocket.close();
        }
    }

    public static void main(String[] args) throws IOException {

        if (args.length != 6) {
            System.err
                    .println("Usage: replay <input_file_location> <server_port> <tweets_per_second> \n" +
                            "<num_seconds> <src_type: 1 for file, 2 for twitter stream>  <word_count_output>");
            System.exit(1);
        }

        String input_file_location = args[0];
        Integer server_port = Integer.parseInt(args[1]);
        Integer tweets_per_second = Integer.parseInt(args[2]);
        Integer num_seconds = Integer.parseInt(args[3]);
        Integer src_type = Integer.parseInt(args[4]);
        String word_count_output = args[5];


        replay r = new replay(input_file_location, server_port, tweets_per_second, num_seconds, src_type, word_count_output);


        r.start_server();
    }
}