package velocity;

import twitter4j.StallWarning;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;
import twitter4j.Status;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.json.DataObjectFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class TweetProducer implements Runnable {
	private BlockingQueue<Tweet> queue;
	private File folderFile;
	private ArrayList<File> fileList = new ArrayList<File>();
	private int dataSourceType;
    private int to_produce;
	
	private StatusListener listener;
	private TwitterStream twitterStream;

	
	public TweetProducer(BlockingQueue<Tweet> q, File ff, int ds_type, int num_tweets_to_produce) {
		this.queue = q;
		this.folderFile = ff;
		this.dataSourceType = ds_type;
        this.to_produce = num_tweets_to_produce;
		
		if (ds_type == 2)
		{
		    	this.listener = new StatusListener(){

		        public void onStatus(Status status) {

                    if (to_produce > 0) {
                        String statusJson = DataObjectFactory.getRawJSON(status);
                        Tweet msg = new Tweet(statusJson);
                        send_tweet(msg);
                    }
                    else
                    {
                        twitterStream.cleanUp();
                        twitterStream.shutdown();
                    }
		        }

		        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}
		        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}
		        public void onException(Exception ex) {
		            ex.printStackTrace();
		            
		        }
				@Override
				public void onScrubGeo(long arg0, long arg1) {				
				}
				@Override
				public void onStallWarning(StallWarning arg0) {				
				}
		    };
		    	    
		    this.twitterStream = new TwitterStreamFactory().getInstance();
		    this.twitterStream.addListener(listener);
		}
	}

	/* Recursively go through files and generate File objects */
	private void listFilesForFolder(final File folder) {

		for (final File fileEntry : folderFile.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				this.fileList.add(fileEntry);
			}
		}
	}

	/*
	 * Goes through json files generated as twitter4j dumps and enqueues them
	 * all
	 */
	private void fetch_from_json_files() {

        BufferedReader reader = null;
        String raw_json_line;

        while (true) {
            for (final File file : fileList) {

                try {
                    reader = new BufferedReader(new FileReader(file));

                    while ((raw_json_line = reader.readLine()) != null) {

					    /* Sanity checking json lines */
                        if (raw_json_line.startsWith("{")) {
                            Tweet msg = new Tweet(raw_json_line);
                            if (to_produce > 0) {
                                send_tweet(msg);
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException e) {
                    }
                }
            }
        }
	}


    /*
     * Goes through json files generated as twitter4j dumps and enqueues them
     * all
     */
    private void fetch_from_json_files_cache() {

        BufferedReader reader = null;
        String raw_json_line;

        //Build up an ArrayList of Tweets to send in memory
        List<Tweet> tweet_list = new ArrayList<Tweet>();

        for (final File file : fileList) {
            try {
                reader = new BufferedReader(new FileReader(file));

                while ((raw_json_line = reader.readLine()) != null) {

					/* Sanity checking json lines */
                    if (raw_json_line.startsWith("{")) {
                        Tweet t = new Tweet(raw_json_line);
                        tweet_list.add(t);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                }
            }
        }

        while (true) {
            for (Tweet t : tweet_list) {
                if (to_produce > 0) {
                    send_tweet(t);
                }
                else
                {
                    return;
                }
            }
        }
    }

	/* Copy Tweet on Q */
	private void send_tweet(Tweet t) {
		try {
			queue.put(t);
            this.to_produce -- ;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/* Signals the consumer that producer is done and time to stop after this */
	private void send_exit_tweet() {
		// adding exit message
		Tweet msg = new Tweet("{---exit---}");
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		listFilesForFolder(folderFile);

        if (dataSourceType == 1) {
			//fetch_from_json_files();
            fetch_from_json_files_cache();
			send_exit_tweet();
		} else if (dataSourceType == 2) {
			fetch_from_streaming_api();
		}

		

		//System.out.println("Producer thread exiting...");

	}


	private void fetch_from_streaming_api() {
			
		this.twitterStream.sample();	
	}

}
