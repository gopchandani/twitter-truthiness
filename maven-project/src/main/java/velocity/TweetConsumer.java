package velocity;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;

public class TweetConsumer implements Runnable {

	private BlockingQueue<Tweet> queue;
	private Socket clientSocket;
	private int tweets_per_second;
	private int num_seconds;
	private WordCounter wc = new WordCounter();
	private String wordCountFilePath;
    private int total_sent;


    public TweetConsumer(BlockingQueue<Tweet> q, Socket cs, int tps, int ns, String wcFilePath) {
		this.queue = q;
		this.clientSocket = cs;
        this.tweets_per_second = tps;
		this.num_seconds = ns;
		wordCountFilePath = wcFilePath;
        total_sent = 0;
	}

	void consumer_loop() {

		/* Acquire a stream for the socket */
		try {
			int send_this_second = 0;
			int seconds_passed = 0;

			PrintWriter out;
			out = new PrintWriter(clientSocket.getOutputStream(), true);

            try {
				Tweet tweet;

				// start with current nano-seconds to create a reference
				long ref_time = System.nanoTime();

				// elasped_time should be almost zero here
				long elasped_time = System.nanoTime() - ref_time;

				/*
				 * Keep going until producer keeps going or replay seconds are
				 * remaining
				 */

				while ((seconds_passed < this.num_seconds)) {

					// Keep writing tweets to socket for a whole second, unless
					// tps limit is hit
					if (elasped_time <= 1000000000
							&& (send_this_second < tweets_per_second)) {

                        tweet = queue.take();
                        if (tweet.get_status_json() == "{---exit---}")
                        {
                            break;
                        }

                        out.println(tweet.get_status_json());
                        out.flush();

						//Throw it to wordcount
						//wc.capture_tweet_text(tweet.get_status_text());

						if (out.checkError()) {
							throw new SocketException(
									"Error transmitting data, remote socket must have died.");
						}

                        send_this_second++;
						elasped_time = System.nanoTime() - ref_time;

					}
                    else
                    {
						/*
						 * Check reasons for precisely why the loop terminated
						 * and say so
						 */
						if (elasped_time < 1000000000) {
                            /*
							System.out
									.println("A whole second was not over yet, sent so far:"
											+ Integer.toString(send_this_second));
											*/

							// do nothing remaining amount of time second...
							long start = System.nanoTime();
							while (start + (1000000000 - elasped_time) >= System
									.nanoTime())
								;
						}

						if (send_this_second < tweets_per_second) {
							System.out
									.println("Could not send all the tweets in the second as required, maxed out at:"
											+ Integer.toString(send_this_second)
											+ " tps");

						}

						// re-init for the next second.
                        total_sent += send_this_second;
                        //System.out.println("total_sent: " + total_sent);
                        send_this_second = 0;
						ref_time = System.nanoTime();
						elasped_time = System.nanoTime() - ref_time;
						
						seconds_passed ++;
					}
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (SocketException e) {
				//System.out.println(e.getMessage());
			} finally {
				clientSocket.close();
				//System.out.println("Closing client socket.");
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void run() {

		consumer_loop();
		
		//wc.write_wordcount_json(wordCountFilePath);
		
		//System.out.println("Consumer thread exiting...");

	}
}
