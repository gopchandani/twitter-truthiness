package velocity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;


class ClientWorker implements Runnable {
	private Socket clientSocket;
	private File folderFile;
	private ArrayList<File> fileList = new ArrayList<File>();
	private int tweets_per_second;

	//Constructor
	ClientWorker(Socket cs, File ff, int tps) {
		this.clientSocket = cs;
		this.folderFile = ff;
		this.tweets_per_second = tps;
	}

	private void listFilesForFolder(final File folder) {    	

		for (final File fileEntry : folderFile.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				fileList.add(fileEntry);
			}
		}
	}
	
	
	private ArrayList<String> parse_kevin_file (File file) {
		System.out.println("Parsing File: " + file.getAbsolutePath());
		BufferedReader reader = null;
		ArrayList<String> tweet_texts = new ArrayList<String>();
		try {
			reader = new BufferedReader(new FileReader(file));
			String line_text;

			while ((line_text = reader.readLine()) != null) {

				if (line_text.startsWith("Text: ")) {
					String tweet_text = line_text.substring(6);
					tweet_texts.add(tweet_text);
					//System.out.println(tweet_text);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
			}
		}
		return tweet_texts;
	}

	private void client_loop() throws IOException
	{
		System.out.println("Client connected.");
		try {

			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

			//Go through each file
			for (final File file : fileList) {
				if (file.isDirectory()) {
					System.out.println("Should not happen!");
				} else {  
					ArrayList<String> file_tweets = new ArrayList<String>();
					file_tweets = parse_kevin_file(file);
					for (String tweet: file_tweets)
					{
						out.println(tweet);
						if(out.checkError())
						{
						    throw new SocketException("Error transmitting data, remote socket must have died.");
						}
						//System.out.println(tweet);
					}

					//Wait before next file
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			

		} catch (SocketException e) {
			System.out.println(e.getMessage());
			
		} finally {
			clientSocket.close();
			System.out.println("Closing client socket.");
		}
	}

	public void run(){
		listFilesForFolder(folderFile);
		//System.out.println(fileList);		
		
		try {
			
			/*Callt the work-horse */
			client_loop();
			
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}