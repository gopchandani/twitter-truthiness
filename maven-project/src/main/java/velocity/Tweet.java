package velocity;

import twitter4j.JSONException;
import twitter4j.JSONObject;

public class Tweet {
	private String status_json;
	private String status_text;

	public Tweet(String json_str) {
		this.status_json = json_str;
		this.status_text = "";
	}

	public String get_status_json() {
		return status_json;
	}
	
	public String get_status_text() {
		
		JSONObject jsonObj;
		try {
			
			jsonObj = new JSONObject(status_json);
			status_text = (String) jsonObj.get("text");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return status_text;
	}
}
