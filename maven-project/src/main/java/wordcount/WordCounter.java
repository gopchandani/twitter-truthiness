package wordcount;

import java.io.*;
import java.util.*;

import twitter4j.JSONException;
import twitter4j.JSONObject;

public class WordCounter {

	private HashMap<String, Integer> words_frequencies = new HashMap<String, Integer>();
	int all_words = 0, unique_words = 0;

	public void capture_tweet_text(String tweet_text) {
		String[] tweet_tokens = tweet_text.split("\\s+");
		for (String token : tweet_tokens) {
			all_words++;

			if (words_frequencies.containsKey(token)) {
				int prev_count = words_frequencies.get(token);
				words_frequencies.put(token, prev_count + 1);
			} else {
				words_frequencies.put(token, 1);
				unique_words++; // unique words count
			}
		}
	}

	public void capture_present_count(String token, Integer count)
	{
		all_words ++ ;
		
		if (words_frequencies.containsKey(token)) {
			words_frequencies.put(token, count);
			
		} else {
			words_frequencies.put(token, count);
			unique_words++; // unique words count
		}
	}
	
	public void print_wordcounts() {
		Object[] key = words_frequencies.keySet().toArray();
		Arrays.sort(key);
		for (int i = 0; i < key.length; i++) {
			System.out.println(key[i] + "= " + words_frequencies.get(key[i]));
		}

		System.out.println("Total Words = " + all_words);
		System.out.println("Unique Words = " + words_frequencies.size());

	}

	public void write_wordcount_json(String outputFileName) {
		
		File currentDir = new File("");
		System.out.println(currentDir.getAbsolutePath());
		
		try {

			JSONObject jObj = new JSONObject(words_frequencies);
			System.out.println(jObj);
			FileWriter file = new FileWriter(outputFileName);
			jObj.write(file);
			file.flush();
			file.close();
			
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}